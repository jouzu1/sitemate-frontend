import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@mui/styles';
import { TextField, Button, Grid } from '@mui/material';
import { addBook } from '../actions/bookActions';
import styles from './AddBookForm.module.css'; 
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(2),
    },
  },
}));

const AddBookForm = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!title.trim() || !description.trim()) return;

    try {
      const response = await axios.post('http://localhost:8080/document/create', {
        title,
        description
      });
      console.log('Response:', response.data);
      // Handle response as needed
    } catch (error) {
      console.error('Error:', error);
      // Handle error as needed
    }

    setTitle('');
    setDescription('');
    window.location.reload(); // Reload tab setelah berhasil menambahkan buku
  };

  

  return (
    <form className={`${classes.root} ${styles.form}`} onSubmit={handleSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Title"
            variant="outlined"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Description"
            variant="outlined"
            multiline
            rows={4}
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="primary">
            Add Book
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default AddBookForm;
