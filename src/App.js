// App.js

import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './reducers/rootReducer';
import BookList from './components/BookList';
import AddBookForm from './components/AddBookForm';
import UpdateBookForm from './components/UpdateBookForm';
import DeleteBookForm from './components/DeleteBookForm';

import { ThemeProvider } from '@mui/material/styles';
import theme from './theme'

// Buat store Redux dengan rootReducer
const store = createStore(rootReducer);

const App = () => {

  return (
    <Provider store={store}>
      <div className="App">
        <h1>Book Manager</h1>
        <ThemeProvider theme={theme}>
          <AddBookForm />
          <UpdateBookForm />
          <DeleteBookForm />
          <BookList />
        </ThemeProvider>
      </div>
    </Provider>
  );
};

export default App;
