import React, { useState } from 'react';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@mui/styles';
import { TextField, Button, Grid } from '@mui/material';
import { deleteBook } from '../actions/bookActions';
import styles from './DeleteBookForm.module.css';

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(2),
    },
  },
}));

const DeleteBookForm = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [title, setTitle] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!title.trim()) return;

    // Kirim permintaan delete ke endpoint dengan Axios
    axios.delete(`http://localhost:8080/document/delete/${title}`)
      .then(response => {
        console.log('Delete successful:', response);
        // Lakukan sesuatu setelah penghapusan berhasil, seperti memperbarui state Redux atau memberikan pesan sukses
        dispatch(deleteBook(title)); // Misalnya, hapus buku dari state Redux
        window.location.reload(); // Refresh halaman setelah permintaan berhasil
      })
      .catch(error => {
        console.error('Delete failed:', error);
      });

    setTitle('');
  };

  return (
    <form className={`${classes.root} ${styles.form}`} onSubmit={handleSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Title"
            variant="outlined"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="secondary">
            Delete Book
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default DeleteBookForm;
