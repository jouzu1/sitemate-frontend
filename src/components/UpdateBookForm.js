import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { makeStyles } from '@mui/styles';
import { TextField, Button, Grid } from '@mui/material';
import { updateBook } from '../actions/bookActions';
import axios from 'axios';
import styles from './UpdateBookForm.module.css'; 

const useStyles = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(2),
    },
  },
}));

export const UpdateBookForm = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [updateTitle, setUpdateTitle] = useState('');
  const [loading, setLoading] = useState(false); // State untuk menampilkan status loading

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!title.trim() || !description.trim() || !updateTitle.trim()) return;

    setLoading(true); // Set loading ke true saat mengirim permintaan

    try {
      const response = await axios.put(`http://localhost:8080/document/update/${updateTitle}`, {
        title,
        description
      });
      console.log(response.data); // Logging the response from the server
      // Dispatch the action to update the book in Redux store if needed
      dispatch(updateBook({ title, description }));
      window.location.reload(); // Reload halaman setelah berhasil memperbarui buku
    } catch (error) {
      console.error('Error updating book:', error);
    } finally {
      setLoading(false); // Set loading kembali ke false setelah permintaan selesai, baik berhasil maupun gagal
    }
  };

  return (
    <form className={`${classes.root} ${styles.form}`} onSubmit={handleSubmit}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Title to Update"
            variant="outlined"
            value={updateTitle}
            onChange={(e) => setUpdateTitle(e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="New Title"
            variant="outlined"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Description"
            variant="outlined"
            multiline
            rows={4}
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <Button type="submit" variant="contained" color="primary" disabled={loading}>
            {loading ? 'Updating Book...' : 'Update Book'}
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}

export default UpdateBookForm;
