import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { makeStyles } from '@mui/styles';
import { Grid, Card, CardContent, Typography, IconButton } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';

const useStyles = makeStyles((theme) => ({
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  title: {
    fontSize: '1.2rem',
    marginBottom: theme.spacing(1),
  },
  description: {
    flex: 1,
    overflow: 'auto',
  },
}));

const BookList = () => {
  const classes = useStyles();
  const [books, setBooks] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:8080/document/read')
      .then(response => {
        setBooks(response.data);
      })
      .catch(error => {
        console.error('Error fetching books:', error);
      });
  }, []);

  return (
    <Grid container spacing={3}>
      {books.map(book => (
        <Grid item xs={12} sm={6} md={4} key={book.id}>
          <Card className={classes.card}>
            <CardContent>
              <Typography className={classes.title} variant="h5" component="div">
                ID: {book.id}
              </Typography>
              <Typography className={classes.title} variant="h5" component="div">
                Title: {book.title}
              </Typography>
              <Typography className={classes.description} variant="body2" color="text.secondary">
                Description: {book.description}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
}

export default BookList;
