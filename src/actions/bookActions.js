export const ADD_BOOK = 'ADD_BOOK';
export const UPDATE_BOOK = 'UPDATE_BOOK';
export const DELETE_BOOK = 'DELETE_BOOK';

export const addBook = (book) => {
  return {
    type: ADD_BOOK,
    payload: book
  };
};

export const updateBook = (book) => {
  return {
    type: UPDATE_BOOK,
    payload: book
  };
};

export const deleteBook = (id) => {
  return {
    type: DELETE_BOOK,
    payload: id
  };
};
